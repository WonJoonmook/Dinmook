<?php

  function ordutf8($string, &$offset) {
    $code = ord(substr($string, $offset,1)); 
    if ($code >= 128) {        //otherwise 0xxxxxxx
        if ($code < 224) $bytesnumber = 2;                //110xxxxx
        else if ($code < 240) $bytesnumber = 3;        //1110xxxx
        else if ($code < 248) $bytesnumber = 4;    //11110xxx
        $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
        for ($i = 2; $i <= $bytesnumber; $i++) {
            $offset ++;
            $code2 = ord(substr($string, $offset, 1)) - 128;        //10xxxxxx
            $codetemp = $codetemp*64 + $code2;
        }
        $code = $codetemp;
    }
    $offset += 1;
    if ($offset >= strlen($string)) $offset = -1;
    return $code;
  }

  function chrutf8($dec) {
      if ($dec < 0x80) {
        $utf = chr($dec);
      }
      else if ($dec < 0x0800){
        $utf = chr(0xC0 + ($dec >> 6));
        $utf .= chr(0x80 + ($dec & 0x3f));
      }
      else if ($dec < 0x010000){
        $utf = chr(0xE0 + ($dec >> 12));
        $utf .= chr(0x80 + (($dec >> 6) & 0x3f));
        $utf .= chr(0x80 + ($dec & 0x3f));
      }
      else if ($dec < 0x200000){
        $utf = chr(0xF0 + ($dec >> 18));
        $utf .= chr(0x80 + (($dec >> 12) & 0x3f));
        $utf .= chr(0x80 + (($dec >> 6) & 0x3f));
        $utf .= chr(0x80 + ($dec & 0x3f));
      }
      else {
        die("UTF-8 character size is more than 4 bytes");
      }
      return $utf;
    }

?>

