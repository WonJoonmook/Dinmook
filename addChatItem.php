<?php
  include_once "header_Path.php";
  include_once "header_UTF8.php";

  $days = (int)$_REQUEST["days"];
  $clientBodyLen = (int)$_REQUEST["clientBodyLen"];
 
  $path = $_FILES["bytes"]["tmp_name"];
  $file = fopen($path, "r");
  $bytes = fread($file, filesize($path));
  fclose($file);

  $path_ChatTable = $days.$path_PostChatTable;
  $path_ChatBody = $days.$path_PostChatBody;

  // 오버뷰 처리
  if ( false == is_file($path_Overview) ) {
    $overviewArr = array();
    for ( $i=0; $i<=$days; $i++ )
        $overviewArr[] = 0;

    $file = fopen($path_Overview, "w");
  }
  else {
    $file = fopen($path_Overview, "r+");
    $overviewJson = fread($file, filesize($path_Overview));

    $overviewArr = json_decode($overviewJson, true);

    $preOverviewLen = count($overviewArr);
    $val = $days +1 - $preOverviewLen;
    if ( $val > 0 ) {
      for ( $i=0; $i<$val; $i++ )
          $overviewArr[] = 0;
    }
    
    $overviewArr[$days] += 1;
    fseek($file, 0);
  }

  $overviewJson = json_encode($overviewArr);
  fwrite($file, $overviewJson);
  fclose($file);
  // ~ 오버뷰 처리


  if ( false == is_dir($days) )
    mkdir( $days, 0777, true );

  $preFP = filesize($path_ChatBody);
  $file = fopen($path_ChatTable, "a+");
  fwrite($file, $preFP."\r\n");
  fclose($file);

  
  $resultBytes = array();
  $bodyFile = fopen($path_ChatBody, "a+");
  if ( $preFP > $clientBodyLen ) {

    fseek($bodyFile, $clientBodyLen);
    $newBytes = fread($bodyFile, ($preFP - $clientBodyLen));

    $unpack = unpack("C*", $newBytes);
    $cnt = count($unpack);
    for ($i=1; $i<= $cnt; $i++ )
      $resultBytes [] = $unpack[$i];
    // $offset = 0;
    // while ($offset >= 0)
    //   $resultBytes[] = ordutf8($newBytes, $offset);

  }
  $unpack = unpack("C*", $bytes);
  $cnt = count($unpack);
  for ($i=1; $i<= $cnt; $i++ )
    $resultBytes [] = $unpack[$i];
  // $offset = 0;
  // while ($offset >= 0)
  //   $resultBytes[] = ordutf8($bytes, $offset);


  
  




  // $bodyFile = fopen($path_ChatBody, "a+");
  // if ( $preFP > $clientBodyLen ) {

  //   fseek($bodyFile, $clientBodyLen);
  //   $newBytes = fread($bodyFile, ($preFP - $clientBodyLen));

  //   $resultBytes = '1'.$newBytes.$bytes;

  // }
  // else {
  //   $resultBytes = '1'.$bytes;
  // }




  fwrite($bodyFile, $bytes);
  fclose($bodyFile);
  

  $result['lensTot'] = 0;
  $result['lens'] = count($resultBytes);
  $result['bytes'] = $resultBytes;
  $result['overviewJson'] = $overviewJson;

  $resultStr = json_encode($result);
  $result['lensTot'] = strlen($resultStr);
  echo json_encode($result);

  
?>
