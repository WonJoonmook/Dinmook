<?php
  $days = (int)$_REQUEST["days"];
  $picName = $_REQUEST["picName"];
  if ( false == is_dir($days) )
    mkdir( $days, 0777, true );

  $dest = $days."/".$picName;
  $result = array();
  if ( false == is_file($dest) ) {
    $result['error'] = 1;
  }
  else {
    $file = fopen($dest, "r");
    $readBytes = fread($file, filesize($dest));
    fclose($file);

    $resultBytes = array();
    $unpack = unpack("C*", $readBytes);
    $cnt = count($unpack);
    for ($i=1; $i<= $cnt; $i++ )
      $resultBytes [] = $unpack[$i];
    
    $result['bytes'] = $resultBytes;
  }
  echo json_encode($result);

?>


