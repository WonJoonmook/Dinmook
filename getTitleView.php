<?php
  include_once "header_Path.php";

  
  if ( false == is_file($path_Titleview) ) {
    $data['error'] = 1;
    $titleView = json_encode($data);
  }
  else {
    $file = fopen($path_Titleview, "r");
    $titleView = fread($file, filesize($path_Titleview));
    fclose($file);
  }
  echo $titleView;
?>
