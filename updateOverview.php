<?php
  include_once "header_Path.php";
  include_once "header_Time.php";


  $flownDate = GetFlownDate(CurTime());
  echo "FlownDate = ".$flownDate."\n";

  
  $overviewArr = array();
  for ( $day=0; $day<=$flownDate; $day++ ) {

    if ( false == is_dir($day) ) {
      $overviewArr[] = 0;
    }
    else {
      $path_ChatTable = $day.$path_PostChatTable;
      if ( is_file($path_ChatTable) ) {
        echo "ChatTable Path = ".$path_ChatTable."\n";
        if ( ($rFile = fopen($path_ChatTable, "r")) ) {
          $line=0;
          while (!feof($rFile)) {
            $buffer = fgets($rFile);
            $line++;
          }
          fclose($rFile);

          $overviewArr[] = (int)$line -1;
        }
        else {
          $overviewArr[] = 0;
          echo "Fail Open Path = ".$path_ChatTable."\n";
        }
      }
      else {
        $overviewArr[] = 0;
      }
    }

  }

  $jsonStr = json_encode($overviewArr);
  $overviewFile = fopen($path_Overview, "w");
  fwrite($overviewFile, $jsonStr);
  fclose($overviewFile);
  echo "Success Update Overview\n";
?>

