<?php
  include_once "header_Time.php";
  include_once "header_Path.php";

  $days = GetFlownDate(CurTime());

  if ( false == is_file($path_Overview) ) {
    $overviewArr = array();
    for ( $i=0; $i<=$days; $i++ )
        $overviewArr[] = 0;

    $overviewJson = json_encode($overviewArr);
    $file = fopen($path_Overview, "w");
    fwrite($file, $overviewJson);

  }
  else {
    $file = fopen($path_Overview, "r+");
    $overviewJson = fread($file, filesize($path_Overview));
  } 
  fclose($file);
  // ~ 오버뷰 처리

  $result['days'] = $days;
  $result['curTime'] = CurTime();
  $result['overview'] = $overviewJson;
  echo json_encode($result);

?>

