<?php

  $days = (int)$_REQUEST["days"];
  $dest = $days."/".$_FILES["bytes"]["name"];
  if ( false == is_dir($days) )
    mkdir( $days, 0777, true );

  if ( is_file($dest) ) {
    $result['error'] = 1;
  }
  else {
    $file = fopen($dest, "w");
    fclose($file);
  
    if ( false == move_uploaded_file($_FILES["bytes"]["tmp_name"], $dest) ) {
      $result['error'] = 12;
      $result['dest'] = $dest;
    }
    else {
      $result['good'] = 1;
    }
  }
  echo json_encode($result);

?>
