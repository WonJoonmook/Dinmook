<?php
  include_once "header_Path.php";

  $days = (int)$_REQUEST["days"];
  $title = $_REQUEST["title"];
 
  if ( false == is_file($path_Titleview) ) {
    $file = fopen($path_Titleview, "w");
    $titleView = array();
  //  $titleView['New'] = 1;
  //  $titleView["Path"] = $path_Titleview;
  }
  else {
    $rfile = fopen($path_Titleview, "r");
    if ( false == $rfile ) {
      $data['error'] = 1;
      echo json_encode($data);
      return;
    }

    $jsonStr = fread($rfile, filesize($path_Titleview));
    $titleView = json_decode( $jsonStr, true );
    fclose($rfile);
    
  //  $titleView['New'] = 0;
  //  $titleView['jsonStr'] = $jsonStr;
  //  $titleView['titleView'] = ($titleView);
    $file = fopen($path_Titleview, "w");
  }
  
  $titleView[$days] = $title;
    
  $resultStr = json_encode($titleView);
  fwrite($file, $resultStr);
  fclose($file);

  echo $resultStr;
?>
