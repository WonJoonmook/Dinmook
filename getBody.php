<?php
  include_once "header_Path.php";
  include_once "header_UTF8.php";

  $days = (int)$_REQUEST["days"];
  $clientBodyLen = (int)$_REQUEST["clientBodyLen"];

  $path_ChatBody = $days.$path_PostChatBody;


  if ( false == is_dir($days) )
    mkdir( $days, 0777, true );

  if ( false == is_file($path_ChatBody) ) {
    $resultBytes = null;
  }
  else {

    $resultFP = filesize($path_ChatBody);
    $readLen = ($resultFP - $clientBodyLen);
    if ( $readLen <= 0 ) {
      $resultBytes = null;
    }
    else {
      $file = fopen($path_ChatBody, "r");
      fseek($file, $clientBodyLen);
      $readBytes = fread($file, $readLen);
      fclose($file);

      $resultBytes = array();
      $unpack = unpack("C*", $readBytes);
      $cnt = count($unpack);
      for ($i=1; $i<= $cnt; $i++ )
        $resultBytes [] = $unpack[$i];
      // $offset = 0;
      // while ($offset >= 0)
      //   $resultBytes[] = ordutf8($readBytes, $offset);
    }
  }
  $result['readLen'] = $readLen;
  $result['bytes'] = $resultBytes;
  echo json_encode($result);

?>


